import random
stamina = 10
dyka = 0
lano = 0

def start():
    global stamina
    print("jsi zvěd ve středověké válce a při tvé misi ukrát plány nepříteli tě chytily a ty musíš utéct a předat všechny informace")
    print("probudil jis se v žaláří a kupodivu jsi celkem odpočatý maš", stamina, "staminy, každá akce tě bude stát nějákou staminu a každé kolo se ti dobije 2 body staminy")
    print("jo a pokud ti dojde stamina tak jis celekm v háji protože nebudeš mít síl use hýbat a tím pádem prohraješ pepehands")
    print("podří se ti obelstít bachaře a ukrást mu klíče a hned jak odejde na obědovou pauzu se rozhodneš utéct takže moc času nemáš")
    print("máš 2 možnoti kam se vydat:")
    print("1 zůstaneš ve sklepení a půjdeš do neznámé místnosti (1- stamina)")
    print("2 nahoru po schodech (-1 stamina)")
    if stamina > 0:
        volba = input("co si vybereš")
        if volba == "1":
            stamina = stamina - 1
            zbrojírna()
        elif volba == "2":
            stamina = stamina - 1
            velkamistnost()
        else:
            print("špatná odpověd zkus to znovu")
            start()
    elif stamina <= 0:
        nostamina()

def zbrojírna():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy")
    print("naběhl jsi do zbrojírny a je tam prázdno, jediné co tam vidíš je hromada velkých zbraní které ti jsou k ničemu")
    print("ale je tam na stalo dýka vezmeš ji? (-3 staminy)")
    print("1 ano")
    print("2 ne")
    if stamina > 0:
        volba = input("co si vybereš?")
        if volba == "1":
            print("čapnul jsi dýku a nezbývá ti nic jiného než se vydat po těch schodech nahoru takže se vydáš po schodech nahoru")
            stamina = stamina - 3
            dyka = dyka + 1
            velkamistnost()
        elif volba == "2":
            print("dýku necháš ležet a už nemáš kam jít takže se vydáš zpátky a půjdeš po schodech nahoru")
            stamina = stamina - 1
            velkamistnost()
        else:
            print("špatná odpověd zkus to znovu")
            zbrojírna()
    elif stamina <= 0:
        nostamina()

def velkamistnost():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a máš", dyka, "dýka")
    print("vyběhl jsi po schodech a ocitl jsi se ve velké místnosti s hodně chodbami 2 v pravo 2 v levo")
    print("některé jsou i dokonce nadepsané a některé mají nadpis oškrábaný takže nejde přečíst kam se vydáš?")
    print("1. do kuchyně v levo (-3 staminy)")
    print("2. chodba levo (-2 staminy)")
    print("3. první  chodba v pravo (-1 staminy")
    print("4. druhá  chodba v pravo (-1 staminy)")
    if stamina > 0:
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 3
            kuchyň()
        elif volba == "2":
            stamina = stamina - 2
            chodbalevo()
        elif volba == "3":
            stamina = stamina -1
            chodbapravo()
        elif volba == "4":
            stamina = stamina - 1
            jídelna()
        else:
            print("špatná odpověd zkus to znovu")
            velkamistnost()
    elif stamina <= 0:
        nostamina()

def kuchyň():
    global stamina
    stamina = stamina + 2
    print("máš", stamina, "a máš", dyka, "dýka")
    print("naběhl jsi do kuchyně ale nikdo tam není co uděláš")
    print("1. scháluješ chlaba co jsi tam našel a půjdeš zpátky (+4 staminy)")
    print("2. necháš chleba chlebem a jdeš zpátky (-1 stamina)")
    if stamina > 0:
        volba = input("co uděláš?")
        if volba == "1":
            stamina = stamina + 4
            velkamistnost()
        elif volba == "2":
            stamina = stamina - 1
            velkamistnost()
        else:
            print("špatná odpověd zkus to znovu")
            kuchyň()
    elif stamina <= 0:
        nostamina()

def jídelna():
    global stamina
    global dyka
    global lano
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("nakoukl jsi do neznámé mistnosti a zjistil jsi že je to jídelna, ale naštěstí si tě nikdo nevšiml protože všichni jsou příliš zaneprázdnění jezením")
    print("jeden se ale už zvedl a začal odcházet a jsi celkem v háji co uděláš")
    if stamina > 0:
        if dyka > 0:
            print("1. počkat si na něj až za sebou zavře dveře a zabodnout ho dýkou (-1 dýka -8 staminy")
            print("2. vylézt na bedny a schovat se tam a doufat že prostě projde a nevšimne si tě (-15 staminy)")
            volba = input("co uděláš")
            if volba == "1":
                print("super zabil jsi ho a hele měl u sebe lano")
                stamina = stamina - 8
                dyka = dyka - 1
                lano = lano + 1
                velkamistnost()
            elif volba == "2":
                print("naštěstí si ničeho nevšiml a jen tak prošel je to v cajku")
                stamina = stamina - 15
                velkamistnost()
            else:
                print("špatná odpověd zkus to znovu")
                jídelna()
        elif dyka == 0:
            print("1. vylézt na bedny a schovat se tam nemá šanci si tě všimnout takže jen projde a nevšimne si tě (-15 staminy)")
            print("2. zkusíš stěstí a schováš se za dveře (-5 stamin)")
            print("3. vezmeš nohy na ramne a budeš doufat že tě nechytí(-10 staminy)")
            print("4. pokusíš se ho zbušit snad to vyjde ale je ozbrojený takže šance je celekm malá (-10 staminy)")
            volba = input("co uděláš")
            if volba == "1":
                stamina = stamina - 15
                velkamistnost()
            elif volba == "2":
                gen = round(random.uniform(1, 2))
                stamina = stamina - 5
                if gen == 1:
                    print("podařilo se ti schovat js safe a vracíš se zpátky")
                    velkamistnost()
                elif gen == 2:
                    print("sakra chytili tě je mi líto")
                    nevyšloto()
            elif volba == "3":
                stamina = stamina - 10
                gen = round(random.uniform(1, 2))
                if gen == 1:
                    print("podařilo se ti utéct zpátky do té velké místnosti")
                elif gen == 2:
                    print("sakra chytily tě je mi líto")
                    nevyšloto()
            elif volba == "4":
                stamina = stamina - 10
                gen = round(random.uniform(1, 4))
                if gen == 1:
                    print("podařilo se ti ho přeprat a omráčit a helemese měl u sebe lano to se bude hodit")
                    lano = lano + 1
                    velkamistnost()
                elif gen > 1:
                    print("nepodařilo se ti ho přeprat a rozkopal ti ksicht")
                    nevyšloto()
                else:
                    print("špatná odpověd zkus to znovu")
                    jídelna()
    elif stamina <= 0:
        nostamina()

def nostamina():
    print("sorry ala máš málo staminy musíš od znova")
    print("1. ano")
    print("2. ne")
    volba = input("co si vybereš?")
    if volba == "1":
        start()
    elif volba == "2":
        print("tak zatím pá pá")
        quit()
    else:
        print("špatná odpověd zkus to znovu")
        nostamina()

def nevyšloto():
    print("bohužel to nevyšlo neměl jsi štěst chceš hrát znovu?")
    print("1. ano")
    print("2. ne")
    volba = input("co si vybereš?")
    if volba == "1":
        start()
    elif volba == "2":
        print("tal zatím pá pá")
        quit()
    else:
        print("špatná odpověd zkus to znovu")
        nevyšloto()

def chodbalevo():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("došel jsi do zatáčky téhle chodby a teď se musíš rozhodnout")
    print("1. vylezeš po žebříku nahoru který vede na hradby (-2 stamina)")
    print("2. budeš pokračovat dále po chodbě (-1 staminy)")
    if stamina > 0:
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 2
            hradbylevo()
        elif volba == "2":
            stamina = stamina - 1
            rozcestilevo()
        else:
            print("špatná odpověd zkus to znovu")
            chodbalevo()
    elif stamina <= 0:
        nostamina()

def chodbapravo():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("došel jsi do zatáčky téhle chodby a teď se musíš rozhodnout")
    print("1. vylezeš po žebříku nahoru který vede na hradby (-2 stamina)")
    print("2. budeš pokračovat dále po chodbě (-1 staminy)")
    if stamina > 0:
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 2
            hradbypravo()
        elif volba == "2":
            stamina = stamina - 1
            rozcestipravo()
        else:
            print("špatná odpověd zkus to znovu")
            chodbapravo()
    elif stamina <= 0:
        nostamina()

def hradbylevo():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("vylezl jsi po žebříku a jsi na jedné ze 4 veěží a všechny propojují hradby ta druhá chodba na začátku asi")
    print("vedla na jednu z nich na to bude asi ta napravo a ta v přední části hradu je celek daleko ale vidíš že na")
    print("ní někdo stojí asi má hlídku ale vypadá celek unaveně takže by se dal nějak přelstít")
    if stamina > 0:
        if dyka == 1:
            print("1. přejdeš na do pření šásti hradu a zabiješ toho unaveného hlídače(-1 dýka, -5 staminy)")
            print("2. pokusíš se ho prostě zhodit z těch hradeb (-8 staminy)")
            volba = input("co si vybereš?")
            if volba == "1":
                stamina = stamina - 5
                dyka = dyka - 1
                print("super podařilo se ti ho zabít a hele měl u sebe kus chleba (+3 staminy)")
                stamina = stamina + 3
                předníhradbavlevo()
            elif volba == "2":
                stamina = stamina - 8
                gen = round(random.uniform(1, 2))
                if gen == 1:
                    stamina = stamina + 3
                    print("podařilo se ti ho zhodit pěkná práce a hele měl u sebe kus chleba (+3 staminy)")
                    předníhradbavlevo()
                elif gen == 2:
                    print("sakra přecenil jses  přepral tě je mi líto")
                    nevyšloto()
            else:
                print("špatná odpověd zkus to znovu")
                hradbylevo()
        elif dyka == 0:
            print("1. moc si na něj nevěříš takže se vrátíš zpátky (-2 staminy)")
            print("2. pokusíš se ho prostě zhodit z těch hradeb (-8 staminy)")
            volba = input("co si vybereš?")
            if volba == "1":
                stamina = stamina - 2
                print("dobrá teda vracíš se po žebříku zpátky")
                chodbalevo()
            elif volba == "2":
                stamina = stamina - 8
                gen = round(random.uniform(1, 2))
                if gen == 1:
                    stamina = stamina + 3
                    print("podařilo se ti ho zhodit pěkná práce a hele měl u sebe kus chleba (+3 staminy)")
                    předníhradbavlevo()
                elif gen == 2:
                    print("sakra přecenil jses  přepral tě je mi líto")
                    nevyšloto()

        else:
            print("špatná odpověd zkus to znovu")
            hradbylevo()

    elif stamina <= 0:
        nostamina()

def hradbypravo():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("vylezl jsi na hradby a rozhlížíš se na tu vež na levo by jsi se asi dostal kdyby jsi šel v te velke chodbe")
    print("do leva, teď se musíš rozhodnou co dál na té věži vepředu nikdo není vy padá to bezpečně")
    if stamina > 0:
        print("1. můžeš se vrátit po žebříku zpátky dolů (-2 staminy)")
        print("2. projdeš na přední věž (-2 staminy)")
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 2
            print("slezl jsi po žebříku zase zpátky")
            chodbapravo()
        elif volba == "2":
            stamina = stamina - 2
            print("úspěšně jsi prošel na přední věž")
            předníhradbavpravo()
        else:
            print("špatná odpověd zkus to znovu")
            hradbypravo()
    elif stamina <= 0:
        print("máš málo staminy je mi líto")
        nostamina()

def rozcestilevo():
    global stamina
    global dyka
    stamina = stamina + 2
    if dyka == 0:
        print("hele na zemi se tu válí dýka vezmu si ji bude se hodit")
        dyka = dyka + 1
        print("máš", stamina, "staminy a", dyka, "dýk")
        rozcestilevo1()
    elif dyka == 1:
        print("máš", stamina, "staminy a", dyka, "dýk")
        rozcestilevo1()

def rozcestilevo1():
    global stamina
    global dyka
    print("teď se musíš rozhodnout kam dál")
    print("1. budeš pokračovat dál rovně (-1 stamina)")
    print("2. vydáš se do prava na nádvoří (-1 stamina)")
    if stamina > 0:
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 1
            podhradamalevo()
        elif volba == "2":
            stamina = stamina - 1
            nádvoří()
        else:
            print("špatná odpověd zkus to znovu")
            rozcestilevo1()
    elif stamina <= 0:
        nostamina()

def rozcestipravo():
    global stamina
    global dyka
    stamina = stamina + 2
    if dyka == 0:
        print("hele na zemi se tu válí dýka vezmu si ji bude se hodit")
        dyka = dyka + 1
        print("máš", stamina, "staminy a", dyka, "dýk")
        rozcestilevo1()
    elif dyka == 1:
        print("máš", stamina, "staminy a", dyka, "dýk")
        rozcestilevo1()


def rozcestipravo1():
    global stamina
    global dyka
    if stamina > 0:
        print("teď se musíš rozhodnout kam dál")
        print("1. budeš pokračovat dál rovně (-1 stamina)")
        print("2. vydáš se do leva na nádvoří (-1 stamina)")
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 1
            podhradamalevo()
        elif volba == "2":
            stamina = stamina - 1
            nádvoří()
        else:
            print("špatná odpověd zkus to znovu")
            rozcestilevo1()
    elif stamina <= 0:
        nostamina()

def předníhradbavlevo():
    global stamina
    global dyka
    global lano
    stamina = stamina + 2
    if stamina > 0:
        print("máš", stamina, "staminy a", dyka, "dýk")
        if lano == 1:
            print("nacházíš se na přední věži, když se podíváš dolů tak to vypadá že by se to tudy dalo slézt dolů")
            print("ale počkat máš předcilano no super, zajistíš lano a úspěšně utíkáš pryč dobrá práce")
            konec()
        elif lano == 0:
            print("teď jsi v přední části hradeb teď se muíš rozhodnout")
            print("1. sejít dolů po žebříku (-1 staminy)")
            print("2. přejít po hradbách k věži na pravo(-1 stamina)")
            volba = input("co si vybereš?")
            if volba == "1:":
                stamina = stamina - 1
                print("dobrá teda slezeš po žebříku dolů")
                podhradamalevo()
            elif volba == "2":
                stamina = stamina - 1
                print("bez potíží jsi přešel k další věží")
                předníhradbavpravo()
            else:
                print("špatná odpověd zkus to znovu")
                předníhradbavlevo()
    elif stamina <= 0:
        nostamina()

def předníhradbavpravo():
    global stamina
    global dyka
    global lano
    stamina = stamina + 2
    print("sakra slyšíš jak někdo z věže za tebou vylézá po žebříku nahoru")
    print("nejpíše jde na hlídku a ty musíš něco udělat")
    if stamina > 0:
        if dyka == 1:
            print("1. rozeběhneš se k němu a zabodneš ho dýkou hned jak vyleze (-1 dýka) (-5 staminy)")
            print("2. rozeběhneš se k němu a pokusíš se ho zbušit ale nevíš jestli se ti to podaří (-6 staminy)")
            print("3. rychle seběhneš po žebříku dolů aby si tě nevšiml (-4 staminy)")
            volba = input("co si vybereš?")
            if volba == "1":
                stamina = stamina - 5
                dyka = dyka - 1
                print("zabodl jsi ho hned jak vylezl nahoru super")
                print("helemese měl u sebe kouek chleba (+5 staminy")
                stamina = stamina + 5
                print("kam ale teď? jsi tak odkud jis přišel takže se rozhodni")
                print("1. půjdeš se po žebříku dolů (-1 stamina)")
                print("2. vrátíš se zase na přední věž a půjdeš po ženříku dolů (-1 stamina)")
                print("3. půjdeš na věž na levo (-1 stamina)")
                volba2 = input("co si vybereš?")
                if volba2 == "1":
                    stamina = stamina - 1
                    chodbapravo()
                elif volba2 == "2":
                    stamina = stamina - 1
                    podhradbamapravo()
                elif volba2 == "3":
                    stamina = stamina - 1
                    hradbylevo()
                else:
                    print("špatná odpověd zkus to znovu")
                    předníhradbavpravo()
            elif volba == "2":
                stamina = stamina - 6
                gen = round(random.uniform(1, 2))
                if gen == 1:
                    print("podařilo se ti ho zbušit")
                    print("a heleme se měl  usebe kus chleba asi chálka na hlídku ale teď už mu je k ničemu tak ho")
                    print("scháluješ (+5 staminy)")
                    stamina = stamina + 5
                    print("kam ale teď? jsi tak odkud jis přišel takže se rozhodni")
                    print("1. půjdeš se po žebříku dolů  (-1 stamina)")
                    print("2. vrátíš se zase na přední věž a půjdeš po ženříku dolů (-1 stamina)")
                    print("3. půjdeš na věž na levo (-1 stamina)")
                    volba2 = input("co si vybereš?")
                    if volba2 == "1":
                        stamina = stamina - 1
                        chodbapravo()
                    elif volba2 == "2":
                        stamina = stamina - 1
                        podhradbamapravo()
                    elif volba2 == "3":
                        stamina = stamina - 1
                        hradbylevo()
                    else:
                        print("špatná odpověd zkus to znovu")
                        předníhradbavpravo()

                elif gen == 2:
                    print("dostal jsi od něj bídu je mi líto")
                    nevyšloto()

            elif volba == "3":
                stamina = stamina - 4
                print("vpořádku jsi slezl po žebříku dolů")
                podhradbamapravo()
            else:
                print("špatná odpověd zkus to znovu")
                předníhradbavpravo()

        elif dyka == 0:
            print("1. rozeběhneš se k němu a pokusíš se ho zbušit ale nevíš jestli se ti to podaří (-6 staminy)")
            print("2. rychle seběhneš po žebříku dolů aby si tě nevšiml (-4 staminy)")
            volba = input("co si vybereš?")
            if volba == "1":
                stamina = stamina - 6
                gen = round(random.uniform(1, 2))
                if gen == 1:
                    print("podařilo se ti ho zbušit")
                    print("a heleme se měl  usebe kus chleba asi chálka na hlídku ale teď už mu je k ničemu tak ho")
                    print("scháluješ (+5 staminy)")
                    stamina = stamina + 5
                    print("kam ale teď? jsi tak odkud jis přišel takže se rozhodni")
                    print("1. vrátíš se po žebříku dolů odkud jsi původně přilezl (-1 stamina)")
                    print("2. vrátíš se zase na přední věž a půjdeš po ženříku dolů (-1 stamina)")
                    print("3. půjdeš na věž na levo (-1 stamina)")
                    volba2 = input("co si vybereš?")
                    if volba2 == "1":
                        stamina = stamina - 1
                        chodbapravo()
                    elif volba2 == "2":
                        stamina = stamina - 1
                        podhradbamapravo()
                    elif volba2 == "3":
                        stamina = stamina - 1
                        hradbylevo()
                    else:
                        print("špatná odpověd zkus to znovu")
                        předníhradbavpravo()
                elif gen == 2:
                    print("dostal jsi od něj bídu je mi líto")
                    nevyšloto()
            elif volba == "2":
                stamina = stamina - 4
                print("vpořádku jsi slezl po žebříku dolů")
                podhradbamapravo()
            else:
                print("špatná odpověd zkus to znovu")
                předníhradbavpravo()
    elif stamina <= 0:
        nostamina()

def podhradamalevo():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    if stamina > 0:
        print("právě se nacházíš pod přední věží hradeb v pravé části hradu a kam teď že")
        print("1. jít na rozcesstí za tebou (-1 stamina)")
        print("2. vylézt po žebříku nahoru (-2 staminy)")
        print("3. jít k hlavní bráně (-1 stamina)")
        volba = input("co si vybereš?")
        if volba == "1":
            stamina = stamina - 1
            rozcestilevo()
        elif volba == "2":
            stamina = stamina - 2
            předníhradbavlevo()
        elif volba == "3":
            stamina = stamina - 1
            hlavníbrána()
        else:
            print("špatná odpověd zkus to znovu")
            podhradamalevo()
    elif stamina <= 0:
        nostamina()

def podhradbamapravo():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("právě se nacházíš pod přední věží hradeb v pravé části hradu a kam teď že")
    print("1. jít na rozcesstí za tebou (-1 stamina)")
    print("2. vylézt po žebříku nahoru (-2 staminy)")
    print("3. jít k hlavní bráně (-1 stamina)")
    volba = input("co si vybereš?")
    if stamina > 0:
        if volba == "1":
            stamina = stamina - 1
            rozcestipravo()
        elif volba == "2":
            stamina = stamina - 2
            předníhradbavpravo()
        elif volba == "3":
            stamina = stamina - 1
            hlavníbrána()
        else:
            print("špatná odpověd zkus to znovu")
            podhradbamapravo()
    elif stamina <= 0:
     nostamina()
def konec():
    print("podařilo se ti uniknout jednou z mnoha možností v mé hře jí ti děkuji že jsi hrál a pokud chceš můžeš si hru")
    print("zahrát znovu a zkusit jinou cestu co říkáš?")
    print("1. hrát znovu")
    print("2. ukončit hru")
    volba = input("co si vybereš?")
    if volba == "1":
        start()
    elif volba == "2":
        quit()
    else:
        print("špatná odpověd zkus to znovu")
        konec()

def nádvoří():
    global stamina
    global dyka
    stamina = stamina + 2
    print("máš", stamina, "staminy a", dyka, "dýk")
    if stamina > 0:
        print("Došel jsi na nadvoří")
        print("Rozhledl jsi se kolem sebe.")
        print("Zjistil jsi že se na tebe dívají všichny straže a král hradu")
        print("Král: Vítej v mé malé hře")
        print('Já:V jaké hře ?')
        print("Král: Jsem rád že jse ptáš")
        print("Král Dostaneš meč a budeš se muset bránít věcem které na tebe pošlu")
        print("Já:A to má být jako výzva zatím co jsem prochazel hrad tak jsem zjstil že tvý straže jsou pěkní tuponi")
        print("Král: Ještě uvidíme a divně se usmál")
        print("1 Zustat a bojovat")
        print("2 Zkusit utéct pryč")

        volba = input("Tvoje rozhodnutí")
        if volba == "1":
            zustatboj()
        elif volba == "2":
            zkusitutect()
        else:
            print("špatná odpověd zkus to znovu")
            nádvoří()
    elif stamina <= 0:
        nostamina()

def zkusitutect():
    global stamina

    print("V hlavě ti utkvěla myšlenka že by jsi mohl zkusit utect otevřenou branou")
    print("Mezitím co král mluvil")
    print("Jsi planoval svůj utěk")
    print("Vyklidnil jsi svou mysl")
    print("Mentalně jsi si připravoval na běh svého života")
    print("Budeš muset běžet jako nikdy")
    print("V hlavě si jsi všechno srovnal")
    print("A jsi připraven běžet")
    print("Jakmile král domluvl")
    print("Tak jsi se rozběhl k otevřené bráně")
    print("V hlavě si jsi řikal:")
    print("Už jsem skoro tam to zvladnu")
    print("A najednou tě trefili lučistnící")
    print("Spadl jsi k zemi a řikal jsi si že za pokus to stálo")
    print("Bohužel tohle ti nevyšlo budeš holt muset bojovat")
    print("1. Vratit se zpatky")
    print("2. Ukončit hru")
    volba = input("Tvoje rozhodnutí")
    if volba == "1":
        vratitsezpět()
    elif volba == "2":
        konechry()
    else:
        print("špatná odpověd zkus to znovu")
        zkusitutect()

def konechry():
    exit()

def vratitsezpět():
    print("Doufal jsem že si vybereš tuto možnost :)")
    nádvoří()

def zustatboj():
    global stamina
    print("Rozhold jsi se zustat a bojovat")
    print("Král se začal bavit se straží")
    print("Král: Zařval nechť výzva započne")
    print("Došli na tebe 2 bachaři")
    print("(Myšlenka) Mám je zabít nebo je ušetřit")
    print("1 zabít bachaře")
    print("2 omračit bachaře")
    volba = input("Tvoje rozhodnutí")
    if volba == "1":
        zabitbachare()
    elif volba == "2":
        omracitbachare()
    else:
        print("špatná odpověd zkus to znovu")
        zustatboj()

def omracitbachare():
    global stamina
    print("Rozhodl jsi se bachaře omračit")
    print("Oba na tobe běželi")
    print("Vyhl jsi se všem jejich utokum")
    print("Prvnímu jsi dal ranu přímo mezi oči a padl k zemi omračen")
    print("Druhý jsem na tebe otočil a sekl")
    print("Ty jsi se hbitě vyhnul a ránu opětoval")
    print("Dal jsi mu dvě rány do břicha a loket mezi oči a omračen padl k zemi")
    print("Král: Co to má být. No nic párat se s tebou nebudu nemám na tebe čas")
    print("Král: Vypusťte draka")
    print("Stojíš tam ani nedýcháš potom co jsi slyšel")
    print("Snažil jsi se uklidnit a připravit se na to co tě čeká")
    priletdraka()

def zabitbachare():
    global stamina
    print("Rozhodl jsi se bachaře zabít")
    print("Oba se po tobě rozběhli")
    print("Hbitě jsi se vyhnul jejím uderum")
    print("Prvnímu bachařovi jsi usekl hlavu")
    print("Druhý  bachař se zalekl")
    print("Ale nevzdal jse")
    print("Agresivně bez rozvahy se po tobě ohnal mečem")
    print("Vyhnul jsi se 5 uderum a uder si opětoval")
    print("Na první ránu jsi mu usekl ruku")
    print("Na podruhé nohu")
    print("A ná třetí finalní jsi mu usekl hlavu a ukazal jsi že není radno jsi s tebou zahrávát")
    print("Král: Co to má být. No nic párat se s tebou nebudu nemám na tebe čas")
    print("Král: Vypusťte draka")
    print("Stojíš tam ani nedýcháš potom co jsi slyšel")
    print("Snažil jsi se uklidnit a připravit se na to co tě čeká")
    priletdraka()

def priletdraka():
    print("Nějak jse ti povedlo uklidnit sám nevíš jak")
    print("Čekáš na to co se stane")
    print("Podival jsi se na oblohu a vídíš jak přiletá drak")
    print("Řikal jsi si že to asi nezvladneš")
    print("Ale neztracel si naději")
    print("Bylo to mladě drake")
    print("Takže nebyl tak velký")
    print("Je tak 2x větší jak ty")
    print("Král: Nemám na tebe čas tak to urychlíme toto je Arnošt můj malý drak")
    print("Král: A ta budeš jeho oběd")
    print("Já: To ještě uvidíme")
    bojsdrakem()

def bojsdrakem():
    print("Stojíš tam jako kámen a přemyšlíš co dělat")
    print("Všiml jsi si že nadvoře jsou nějaké barely a bedny")
    print("Napadlo tě že pokuď by drak začal chrlit oheň tak se za ně mužeš schovat")
    print("V hlavě jsi si spočital všechny šance ")
    print("Mysliš si že máš docela velkou šanci na přežití")
    print("Drak jse začal hýbat ")
    print("Připravuje se chrlit oheň co uděláš")
    print("1 Schovat se za krabice")
    print("2 Zustat stát a nic nedělat")
    print("3 Začít běhat kolem aby jsi draka zmatl")
    volba = input("Tvoje rozhodnutí")
    if volba == "1":
        schovatkrabice()
    elif volba == "2":
        udelatnic()
    elif volba == "3":
        zacitbehat()
    else:
        print("špatná odpověd zkus to znovu")
        bojsdrakem()

def schovatkrabice():
    print("Schovaj jsi se za krabici")
    print("Drak začal chrlit")
    print("Cítíl jsi to teplo kolem tebe")
    odpoved = input('Chceš ještě čekat v krytu ? ')
    while odpoved != 'Ne':
        print('Čekáš dale v krytu')
        odpoved = input('Až budeš chtít vylezt napiš Ne')
    print("Vylezl jsi z krytu a drak je zmatený")
    print("Vycítíl jsi příležitost")
    print("Naběhl jsi na draka jako za mlada")
    print("A usekl mu hlavu")
    print("Král: Ne arnošte. Lučistnici popravte ho")
    print("Padáš k zemi ")
    print("Bohužel jsi zemřel")
    print("Zkus to znovu :D")
    konechry()

def udelatnic():
    print("Zustal jsi stát na mistě a nic nedělat")
    print("Drak tě zpražil jak nikdy")
    print("Stal jse z tebe potočenej černej krekr")
    print("A bohužel toto nebyla správná volbla")
    print("Doufám že na podruhé se ti to už povede")
    bojsdrakem()

def zacitbehat():
    print("Zpanikařil jsi a začal jsi běhat kolem celého nadvoří")
    print("Drak jse tam tocil jako beyblade a snažil jse na tebe zamiřít ")
    print("Tak po 5 kole to vzdal a motala jsem mu hlava")
    print("A ty jsi využil situace")
    print("Zatím co se drak vzpamatovával")
    print("Tak ty jsi ani nezaváhal a rozběhl jse po něm")
    print("Opětoval jsi utok")
    print("Povedlo se ti draka osedlat")
    print("A draka magickým zposobem ochočit")
    print("Poté co jse ti to povedlo")
    print("Král: To je nemožné jak se ti to povedlo ")
    print("Já: Mám prostě svuj šarm teďka zaplatíš nejvysší cenu")
    print("Král: Nechce se mi věřit že takový ubožák jak ty vyhral")
    print("Poté co král domluvil nakazal jsi drakovy aŤ všechny lidi co jsou na hradbách se smaží na uhel")
    print("A tvůj potočenej drak jménem Arnošt poslechl")
    print("Nakonec to tam vypadalo jak kdyby padal černej sníh")
    print("Nakonec jsi se s´svým novým drakem Arnoštem odletěl a žil happy život")
    konechry()

def konechry2():
    print("Tak tohle je asi nejvíc ujetej konec co tu mám")
    print("Doufám že jse ti hra líbíla")
    print("A užil jsi si jí")
    print("Já a Arnošt se s tebou loučíme")
    print("Třeba se uvidíme u druhého dílu")
    print("Pro ukončení hry zmačkni 1")
    volba = input("Konec")
    if volba =="1":
        exit()
    else:
        print("špatná odpověd zkus to znovu")
        konechry2()

def hlavníbrána():
    global stamina
    global dyka
    print("máš", stamina, "staminy a", dyka, "dýk")
    print("naběhl jsi k hlavní bráně a tam je třeba tak 1 000 vojáků")
    print("nemáš že vůbec tušení co pro jak nebo proč ale jsou tam")
    print("najednou se všichni rozestoupily a udělaly 2 řady naproti sobě a ty jsi stál úplně na začátku")
    print("najednou tě zaslepilo nějáké světlo a ty jsi nevěděl co se děje")
    print("po chvíly světlo ustoupilo a před tebou stál gandalf na bílém koni")
    print("vypadlo to jako kdyby chtěl jít do hradu a ty jsi mu stál přímo v cestě ale ono to tak nevypadalo")
    print("omo to tak bylo ale pak jsis vzpoměl že gandalf je tvůj chábr a že jsi mu dealoval tabák do dýmky on tě")
    print("hned poznal zesedl z koně a pozdravili jste se")
    print("mezi tím všichni vojáci absolutně netušily co se děje a ty jsi ve zkratce vysvětlil gandalfovi proč jso tady")
    print("a po krátké konverzaci s ním ti došlo že gandalfovi nalhaly že půjde o mírové jednání o magii ale přitom")
    print("ho chtěli zabít protože ve válce byl na straně tvého království")
    print("hned jak vám to došlo tal se na vás všichni vrhli a bleskovou rychlostí jsi se s gandalfem přemístil")
    print("kousek od království do nedalekého lesa a začaly jste plánovat jak se jim pomstít")
    print("1. nechat je být a jít do háje")
    print("2. povolat tvé království a zbušit je")
    print("3. dostaneš rychlo kurz kouzlení od gandalfa a roztočíte spolu celej hrad")
    volba = input("co si vybereš?")
    if volba == "1":
        celkemnoob()
    elif volba == "2":
        cekaninaarmadu()
    elif volba == "3":
        rychlokurzkouzleni()
    else:
        print("špatná odpověd zkus to znovu")
        hlavníbrána()

def celkemnoob():
    print("no tak nic no škoda mohla výt švanda")
    print("ale pokud chceš můžeš si zahrát hru odznova")
    print("1. hrát odznova")
    print("2. vrátit se k volbě")
    print("3. ukončit hru")
    volba = input("co si vybereš?")
    if volba == "1":
        start()
    elif volba == "2":
        hlavníbrána()
    elif volba == "3":
        exit()
    else:
        print("špatná odpověd zkus to znovu")
        celkemnoob()

def rychlokurzkouzleni():
    print("okey nemáme čas stihnu tě naučit ovládat jen jeden typ kouzel co si vybereš?")
    print("1 oheň")
    print("2. voda")
    print("3. blesky")
    print("4. země")
    print("5. vítr")
    print("6. led")
    volba = input("takže co to bude?")
    if volba == "1":
        uceniohen()
    elif volba == "2":
        ucenivoda()
    elif volba == "3":
        uceniblesky()
    elif volba == "4":
        ucenizemě()
    elif volba == "5":
        ucenivitr()
    elif volba == "6":
        uceniled()
    else:
        print("špatná odpověd zkus to znovu")
        rychlokurzkouzleni()

def uceniohen():
    print("okey takže se chceš naučit oheň já gandalf tě tedy naučím jak ovládat oheň")
    print("základ nebuď dement a nepsal sebe nebo mě ostatní můžeš")
    print("funguje to tak že vezmeš oheň")
    print("hodíš full soustředění na ten oheň")
    print("pak bum bác děláš čáry mára po kočáry")
    print("po tak 10 minutách máchání ohněm ve vzduchu jsi zvládl oheň ovládnout")
    print("teď tam naběhnem a všichni dostanou bídu")
    print("telportovaly jste se s gandalfem doprostřed nádvoří a začaly jste je všechny kosit")
    print("gandalf tam roztáčel kouzla jak za mlada a ty")
    print("ty jsi tam roztočil pořádný ohnivý tornádo o nejvíc jste to s gandalfem zandali")
    print("po urputném boji jste to tam celý spálili na popel gandalf vykouzlil 2 jednorožce a na nich jste odletěli domů")
    koneckouzleni()

def ucenivoda():
    print("okey takže chceš naučit ovládat vodu oukekj chápu já gandalf tě to naučím jak vodu ovládat")
    print("zákal je abys uměl pravat ty demente dál neutopit mě asi sebe ostatní můžeš")
    print("funguje to tak že si stoupneš k vodě plně se na ni soustřeíš")
    print("a pak s ní začneš hýbat")
    print("po asi 10 minutách mácháním rukama u vody se ti podařilo plně ovládnout vodu tak je jdem zbušit ne??")
    print("gandalf teleportoval tebe i sebe doprostřed hradiště a začali jste to tam roztáčet")
    print("gandalf tam házel kouzla jak za mlada a ty jsi způsoboval povodně největšího kalibru")
    print("po nějáké době intenzivního boje jsi tam všechny utopil a celej hrad byl v troskách")
    print("gandalf vykouzlil 2 jednorožce a na nich jste odletěli domů")
    koneckouzleni()

def uceniblesky():
    print("tak ty se chceš naučit používat blesky jo? to bude ještě sranda protže ovládat blesky není vůbec lehký")
    print("základ je umět mířit pokud umíš střílet z luku tak je to super pokud ne tak to budeš mít trošku těžký")
    print("základ je zapojit full focus a chargnout v sobě energii a pak ji vypustit skrz prsty ven")
    print("po asi 15 minutách intenzivního chargování a vypoštění jsi hravě ovládl blesky")
    print("byly jste ready je rozotičit takže jsi se s gandalfem teleportoval do prostřed hradiště")
    print("zatočil jsi tam bleskoví tornádo a už to lítalo")
    print("gandalf to tam mezitím kouzlil na jednorožcovi")
    print("po intentivním souboji jste to tam potočily a hradby byly úplně na padrť")
    print("gandalf ti taky vykouzlil jednorožce a spolu jste odletěli domů")
    koneckouzleni()

def ucenizemě():
    print("takže se chceš naučit ovládat zemi jo tak to je celekm izi jdu tě to naučit")
    print("základ nebýt ndebil ayo jejda tak to asi nepůjde no je mi líto musíš si vybrat něco jinýho no")
    print("1 oheň")
    print("2. voda")
    print("3. blesky")
    print("4. vitr")
    print("5. led")
    volba = input("co si vybereš?")
    if volba == "1":
        uceniohen()
    elif volba == "2":
        ucenivoda()
    elif volba == "3":
        uceniblesky()
    elif volba == "4":
        ucenivitr()
    elif volba == "5":
        uceniled()
    else:
        print("špatná odpověd zkus to znovu")
        ucenizemě()

def ucenivitr():
    print("tak pojď jdem se naučit ovládat vítr to je úuplně izi")
    print("v rychlosti musíš dát jenom pěstí vzduchu předat mu svoji energii a on se vyšle dál")
    print("tak 15 minut jsi tam mlátil do vzduchu (do slova) jak dement ale naučil jsi se to")
    print("díky ovládání větru jsi sebe a gandalfa odnesl doprotřed hradu a začali ste to tam točit jak páni")
    print("házel jsi tak ze všema jak na kolutoču")
    print("po intenzivním souboji jsti to tam naposledy roztočil a eozházel trosky hradu po okolí")
    print("gandalf ti zatleskal a spolu jste odletěli domů si dát kvalitní dýmku")
    koneckouzleni()

def uceniled():
    print("Rozhl jsi se naučit bojové umění ledu od mocného gandalfa lédového mistra v těžké váze")
    print("Musíš urovnat všechny svoje natočený chakry")
    print("Až se ti to povede musíš začít myslel na Eskimaky")
    print("Ne ty ledńáčky ale lidi jo ")
    print("Poté musíš udělat tuhle bojovou formaci")
    print("Poté jen plivní třikrat se otoč jako beyblade")
    print("A začni mávat rukama jak nějakej magor co ví co dělá a přitom nevíš")
    print("A tam kam budeš mířit poletí ledové kly")
    print("Tak pojď jdeme jím nakopat zadnice")
    print("Portli jste se do hradiště")
    print("Začal jsi metat let kolem sebe jako neohrožený bojovník")
    print("Sice jsi ze začatku trefil tak zeď a možná nějaký stojící cápka co zrovna pil pivo")
    print("Ale to nevadí po chvílí se z tebe stal americký sniper")
    print("A trefil jsi každou ránu")
    print("Gandalf na tebe hleděl a nemohl tomu uvěřit")
    print("Měl něco v planu ale nevěděl jsi co")
    print("Po 20 minutach vrhání ledu bylo po všem bitva byla dobojována")
    print("Gandalf tě nenapadně portl do areny a vyzval tě na 1v1 protože jsi byl lepší v metání ledu")
    print("Gandalf se připravoval")
    print("Ty jsi byl už rozehřaty tak že jsi přemyšlel co ten děda má za problem")
    print("Po jedné minutě boje co jsi se jen vyhýbal")
    print("Tak doletěl drak Arnošt z jiného konce")
    print("A strašně gandalfa schaloval")
    print("Poté jsi doběhl k arnoštovi a začal jsi ho hladit a chavalit jak dobrý drak to je")
    print("Pak jste spolu odletěli do nejbližší hospody a ožrali se")
    print("A toť je konec Gandalfa ledového mistra v těžké váze")
    print("Docela šilený konec ale už mí došli nápady")
    print("Takže doufám že to vemete s humorem paní učitelko :D")

def cekaninaarmadu():
    print("gandalf zatočil kouzlo a zavolal domů do království ti hned jak se zprávu dozvěděl tak přifrčela celá armáda")
    print("takže jste začaly plánovat útok")
    print("po nějáke dobrě jste se na to vykašlali protože jste ůuplně blbí a postě jste je šli rozkopak")
    print("gandalf tě mezitím naučil nějáký ty kouzla a šli jste to potočit")
    print("po intezivním souboji jste to rozkopaly jak hrad z písku")
    print("tímto jste válku vyhrály a šli jste spokojeně domů")
    print("doma tě pak povíšily na zástupce krále a s gandalfem se z vás stali mega chábři")
    konec()

def koneckouzleni():
    print("tak to vidíš naučil jsi se kouzlit a s gandalfem jste teď nejlepší chábři")
    print("ká ti děkuji že jsi hrál moji hru")
    print("1. hrát od znova")
    print("2. ukončit hru")
    volba = input("co si vybereš?")
    if volba == "1":
        start()
    elif volba == "2":
        exit()
    else:
        print("špatná odpověd zkus to znovu")
        koneckouzleni()

start()
